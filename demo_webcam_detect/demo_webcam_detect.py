import cv2;
import sys;
from time import sleep

class webcam():
	def __init__(self, iCamNum, bCamColor):
		self.iCamNum = iCamNum;
		self.bCamColor = bCamColor;
		self.bCamStatus = False;
		self.iCamLoadTimes = 0;
		pass;



	def detect(self, imgFrame, sDetectModel):
		Cascade = cv2.CascadeClassifier(sDetectModel);

		# convert RGB to Gray
		if self.bCamColor:
			imgGray = cv2.cvtColor(imgFrame, cv2.COLOR_BGR2GRAY);
		else:
			imgGray = imgFrame;

		# face detect
		detect = Cascade.detectMultiScale(
			imgGray, 
			scaleFactor=1.1, 
			minNeighbors=5, 
			minSize=(30, 30)
		);

		#draw a rectangle around the faces
		for (x, y, w, h) in detect:
			cv2.rectangle(imgFrame, (x,y), (x+w, y+h), (0, 255, 0), 2);

		return imgFrame;



	def control(self, bDetect):
		video_capture = cv2.VideoCapture(self.iCamNum);
		
	
		# check camera connected
		while True:
			if self.iCamLoadTimes >= 3:
				print('Please check your camera!');
				break;
			elif not video_capture.isOpened():
				print('Unable to load camera.');
				sleep(3);
				self.iCamLoadTimes += 1;
				pass;
			else:
				self.bCamStatus = True;
				print('key "q" is quit.');
				break;

		while self.bCamStatus:
			# capture frame-by-frame
			ref, imgFrame = video_capture.read();
			
			# convert RGB to Gray
			if not self.bCamColor:
				imgFrame = cv2.cvtColor(imgFrame, cv2.COLOR_BGR2GRAY);

			# detect
			if bDetect:
				sDetectModel = "haarcascades/haarcascade_frontalface_default.xml";
				imgFrame = self.detect(imgFrame, sDetectModel);

			# show video
			cv2.imshow('Video', imgFrame);

			# break while loop
			if cv2.waitKey(1) & 0xFF == ord('q'):
				break;

		# When everything is done, release the capture
		video_capture.release();
		cv2.destroyAllWindows();


cWebcam = webcam(0, True);
cWebcam.control(True);
