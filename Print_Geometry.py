class Print_Geometry_Hollow():
	def __init__(self):
		pass;

	def Hollow_Base(self, size):
		if size < 1:
			return "*";
		else:
			return "*" + " "*size + "*";

	def Isosceles_Upper(self, size):
		result = "";
		for i in range(0, size-1):
			result += " "*(size-(i+1));
			result += self.Hollow_Base(i*2-1);
			result += "\n";
		result += "*"*(size*2-1) + "\n";
		return result;

	def Isosceles_Lower(self, size):
		result = "";
		result += "*"*(size*2-1) + "\n";
		for i in reversed(range(0, size-1)):
			result += " "*(size-(i+1));
			result += self.Hollow_Base(i*2-1);
			result += "\n";
		return result;

	def Rhombus(self, size):
		result = "";
		for i in range(0, size):
			result += " "*(size-(i+1));
			result += self.Hollow_Base(i*2-1);
			result += "\n";
		for i in reversed(range(0, size-1)):
			result += " "*(size-(i+1));
			result += self.Hollow_Base(i*2-1);
			result += "\n";
		return result;

		

class Print_Geometry():
	def __init__(self):
		pass;

	def Isosceles_Upper(self, size):
		result = "";
		for i in range(0, size):
			result += " "*(size-i-1);
			result += "*"*((i+1)*2-1);
			result += "\n";
		return result;

	def Isosceles_Lower(self, size):
		result = "";
		for i in reversed(range(size)):
			result += " "*(size-i-1);
			result += "*"*((i+1)*2-1);
			result += "\n";
		return result;

	def Rhombus(self, size):
		result = "";
		# Isosceles_upper
		for i in range(0, size):
			result += " "*(size-i-1);
			result += "*"*((i+1)*2-1);
			result += "\n";
		# Isosceles_lower
		size -= 1;
		for i in reversed(range(size)):
			result += " "*(size-i);
			result += "*"*((i+1)*2-1);
			result += "\n";
		return result;



	




Geometry = Print_Geometry();
Geometry_Hollow = Print_Geometry_Hollow();

print(Geometry.Isosceles_Upper(5));
print(Geometry.Isosceles_Lower(5));
print(Geometry.Rhombus(5));

print(Geometry_Hollow.Isosceles_Upper(5));
print(Geometry_Hollow.Isosceles_Lower(5));
print(Geometry_Hollow.Rhombus(5));