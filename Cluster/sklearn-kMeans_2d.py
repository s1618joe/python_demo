import numpy as np;
import matplotlib.pyplot as plt;
from sklearn.cluster import KMeans;
np.random.seed(1);

n = 1000; # number of sample point
d = 2; # dimensions 
k = 3; # number of clustering

# create random sample
data1 = np.random.uniform(-10, 10, [int(n/k), d]);
data2 = np.random.uniform(0, 20, [int(n/k), d]);
data3 = np.random.uniform(-20, 0, [int(n/k), d]);

# graph of sample distribution
plt.figure(0);
plt.scatter(data1[:,0], data1[:,1], s=5);
plt.scatter(data2[:,0], data2[:,1], s=5);
plt.scatter(data3[:,0], data3[:,1], s=5);
plt.show();

# concatenate data
data = np.concatenate((data1, data2, data3), axis=0);

# K-Means clustering
kmeans = KMeans(n_clusters=k, 
                init='k-means++', # k-means++, random
                n_init=10, 
                max_iter=300, 
                tol=1e-10, 
                precompute_distances='auto', # auto
                verbose=0, 
                random_state=None, 
                copy_x=True, 
                n_jobs=None, 
                algorithm='auto').fit(data);
kmeans_predict = kmeans.predict(data);

print("Center is \n", kmeans.cluster_centers_); # center of clustering
print("Sum of distance ", kmeans.inertia_); # sum of the distance of sample to center
print("Times of iterate is ", kmeans.n_iter_); # times of iterate

# graph of clustering
plt.figure(1);
plt.scatter(data[:,0], data[:,1], c=kmeans_predict, s=5);
plt.scatter(kmeans.cluster_centers_[0,:][0], kmeans.cluster_centers_[0,:][1], c='r', s=50);
plt.scatter(kmeans.cluster_centers_[1,:][0], kmeans.cluster_centers_[1,:][1], c='r', s=50);
plt.scatter(kmeans.cluster_centers_[2,:][0], kmeans.cluster_centers_[2,:][1], c='r', s=50);
plt.show();